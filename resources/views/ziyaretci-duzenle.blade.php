@extends('layouts.layout')

@section('title')
    Ziyaretçi Düzenleme Sayfası
@endsection

@section('content')
   
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Ziyaretçi Düzenleme
      </h1>    
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">

            @include('errors')

          <div class="box box-primary">            
            {{-- <form action="{{ route('ziyaretci.update',$ziyaretci->id) }}" method="PUT"> --}}
              {{-- {!! Form::model($ziyaretci,['route' => ['ziyaretci.update',$ziyaretci->id], 'method' => 'PUT']) !!} --}}
              <form action="{{ route('ziyaretci.update',$ziyaretci->id) }}" method="POST">

                @csrf
                @method('PUT')

            <div class="box-body">

                <div class="form-group">
                  <label for="exampleInputEmail1">Ziyaretçi Ad</label>
                  <input type="text" name="ad" class="form-control" id="exampleInputEmail1" value="{{$ziyaretci->ad}}" placeholder="Ad giriniz..">
                </div>

                <div class="form-group">
                  <label for="exampleInputPassword1">Ziyaretçi Soyad</label>
                  <input type="text" name="soyad" class="form-control" id="exampleInputPassword1" value="{{$ziyaretci->soyad}}" placeholder="Soyad giriniz..">
                </div>

                <div class="form-group">
                    <label for="exampleInputPassword1">Ziyaretçi Kimlik Belgesi Türü</label>
                    <input type="text" name="belge" class="form-control" id="exampleInputPassword1" value="{{$ziyaretci->belge}}" placeholder="Kimlik belge türü giriniz.">
                </div>

                <div class="form-group">
                  <label for="exampleInputPassword1">Ziyaretçi Kimlik Numarası</label>
                  <input type="text" name="kimlik_no" class="form-control" id="exampleInputPassword1" value="{{$ziyaretci->tc_kimlik}}" placeholder="Belge Numarasını boşluk bırakmadan giriniz.">
                </div>

                <div class="form-group">
                  <label for="exampleInputPassword1">Ziyaretçi Telefon</label>
                  <input type="text" name="telefon" class="form-control" id="exampleInputPassword1" value="{{$ziyaretci->telefon}}" placeholder="Telefon Numarasını boşluk bırakmadan giriniz.">
                </div>

                <div class="form-group">
                  <label for="kurum">Ziyaret Edilen Birim</label>
                  <select id="kurum" name="kurum" class="form-control">
                    @foreach ($kurumlar as $kurum)
                      <option value="{{ $kurum->id }}" {{$kurum->id == old('kurum_id',$ziyaretci->kurum_id) ? 'selected' : '' }}>{{ $kurum->kurum_adi }}</option>             
                    @endforeach
                  </select>
                </div>

                <div class="form-group">
                    <label for="birim">Ziyaret Edilen Müdürlük</label>
                    <select id="birim" name="birim" class="form-control">
                      <option value="">Müdürlük Seçiniz</option>
                        @foreach ($alt_birimler as $alt_birim)
                          <option value="{{ $alt_birim->id }}" {{$alt_birim->id == old('birim_id',$ziyaretci->birim_id) ? 'selected' : '' }}>{{ $alt_birim->kurum_adi }}</option>                 
                         @endforeach 
                    </select>
                </div>
                @if ($ziyaretci->seflik_id)
                  <div class="form-group">
                    <label for="seflik">Ziyaret Edilen Şeflik</label>
                    <select id="seflik" name="seflik" class="form-control">
                        @foreach ($seflikler as $seflik)
                          <option value="{{ $seflik->id }}" {{$seflik->id == old('seflik_id',$ziyaretci->seflik_id) ? 'selected' : '' }}>{{ $seflik->kurum_adi }}</option>                 
                        @endforeach 
                    </select>
                  </div>
                @else
                  <div id="sef_div" style="display:none;" class="form-group">
                    <label for="seflik">Ziyaret Edilen Şeflik</label>
                    <select id="seflik" name="seflik" class="form-control">
                        <option value="">Şeflik Seçiniz</option>             
                    </select>
                  </div>
                @endif

                <div class="form-group">
                  <label for="exampleInputPassword1">Ziyaret Edilen Kişi</label>
                  <input type="text" name="ziyaret_edilen" class="form-control" id="exampleInputPassword1" value="{{$ziyaretci->ziyaret_edilen}}" placeholder="Ziyaret edilecek kişiyi giriniz..">
                </div>

                <div class="form-group">
                  <label for="exampleInputPassword1">Ziyaret Sebebi</label>
                  <input type="text" name="ziyaret_sebebi" class="form-control" id="exampleInputPassword1" value="{{$ziyaretci->ziyaret_sebebi}}" placeholder="Ziyaret sebebi giriniz..">
                </div>

                <div class="form-group">
                    <label for="exampleInputPassword1">Ziyaretçi Firma Adı</label>
                    <input type="text" name="firma" class="form-control" id="exampleInputPassword1" value="{{$ziyaretci->firma}}" placeholder="Ziyaretçinin firma adını giriniz giriniz (Zorunlu Değil).">
                </div>

                <div class="form-group">
                    <label for="exampleInputPassword1">Ziyaretçi Araç Plakası</label>
                    <input type="text" name="plaka" class="form-control" id="exampleInputPassword1" value="{{$ziyaretci->arac_plaka}}" placeholder="Ziyaretçinin araç plakasını giriniz (Zorunlu Değil).">
                </div>
              
                <div class="form-group">
                  <label for="kart">Kart Numarası</label>
                  <select id="kart" name="kart-no" class="form-control">
                      <option value="0">Kart seçiniz.</option>
                    @foreach ($kartlar as $kart)
                      <option value="{{ $kart->id }}" {{$kart->id == old('kart_no',$ziyaretci->kart_no) ? 'selected' : '' }}>{{ $kart->kart_adi }}</option>
                    @endforeach
                  </select>
                </div>

            </div>

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Güncelle</button>
                </div>
              </form>
              {{-- {!! Form::close() !!} --}}
          </div>
        </div>       
      </div>
    </section>
  </div>

  <script type="text/javascript">

    $('#kurum').on('change',function(){
        var kurum = $(this).val();    
        $("#birim").empty();
        $("#birim").append('<option value="">Birim Seçiniz</option>');
        if(kurum){
            $.ajax({
               type:"GET",
               url:"{{url('admin/birim-getir')}}?kurum_id="+kurum,
               success:function(res){               
                if(res){
                    $.each(res,function(key,value){
                        $("#birim").append('<option value="'+key+'">'+value+'</option>');
                    });
               
                }else{
                   $("#birim").empty();
                }
               }
            });
        }else{
            $("#birim").empty();
        }
            
       });


       $('#birim').on('change',function(){

        var birim = $(this).val();

        $("#seflik").empty();
        $("#seflik").append('<option value="">Şeflik Seçiniz</option>');

        if( birim == 52 || birim == 53 || birim == 54 || birim == 55 || birim == 56 || birim == 57 || birim == 58)
        {
          $('#sef_div').removeAttr('style');

          $.ajax({
            type:"GET",
            url:"{{url('admin/birim-getir')}}?kurum_id="+birim,
            success:function(res){               
              if(res){
                  $.each(res,function(key,value){
                      $("#seflik").append('<option value="'+key+'">'+value+'</option>');
                  });
            
              }else{
                $("#seflik").empty();
              }
            }
          });
        }
        else
        {
          $('#sef_div').css('display','none'); 
        }
        }
        );
    
    
    </script>
    


@endsection
