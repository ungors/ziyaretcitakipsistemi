@extends('layouts.layout')

@section('title')
    Kartlar Sayfası
@endsection

@section('content')

<div style="float:right; margin:15px 15px 22px 0;"><a  href="{{route('kart.create')}}" class="btn btn-success">Kart Ekle</a></div>

<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Kartlar
          </h1>
        </section>
        <!-- Main content -->
        
        <section class="content">
          
          <!-- /.row -->
          <div class="row">
            <div class="col-xs-12">
             @if(count($kartlar) > 0)
              <div class="box">
                <div class="box-header">
                  <div class="box-tools">
                   
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <thead>
                        <tr>
                            <th width="50%">Kart Adı</th>
                            <th width="5%">Düzenle</th>
                          </tr>
                        </thead>
                    <tbody>

                      @foreach ($kartlar as $kart)
                      <tr>
                          <td>{{$kart->kart_adi}}</td>
                          <td>
                            <a class="btn btn-primary" href="{{route('kart.edit',$kart->id)}}">Düzenle</a>
                         </td>
                         {{-- <td>
                            {!! Form::model($kart,['route' => ['kart.destroy',$kart->id],'method' => 'DELETE']) !!}
                                <button type="submit" value="Sil" class="btn btn-danger" onclick="javascript:return confirm('Silinsin mi?');">Sil</button>
                            {!! Form::close() !!}                         
                        </td> --}}
                      </tr>
                      @endforeach
                      
                    </tbody>
                  </table>
                  {{ $kartlar->links() }}
                </div>
                <!-- /.box-body -->
              </div>
              @else
              <br>
                <div class="alert alert-warning">Kayıt bulunamadı.</div>
              @endif
      
              <!-- /.box -->
            </div>
          </div>
        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
    

@endsection