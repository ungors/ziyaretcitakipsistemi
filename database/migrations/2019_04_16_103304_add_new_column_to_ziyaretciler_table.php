<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNewColumnToZiyaretcilerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ziyaretciler', function (Blueprint $table) {
            $table->string('belge');
            $table->string('firma')->nullable();
            $table->string('arac_plaka')->nullable();
            $table->integer('kart_no');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ziyaretciler', function (Blueprint $table) {
            $table->dropColumn('belge');
            $table->dropColumn('firma');
            $table->dropColumn('arac_plaka');
            $table->dropColumn('kart_no');
        });
    }
}
