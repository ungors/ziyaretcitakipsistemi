<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZiyaretciLog extends Model
{
    protected $table = 'ziyaretci_log';
    public $primaryKey = 'id';
    public $timestamps = true;
}
