<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ziyaretci_log', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ad');
            $table->string('soyad');
            $table->integer('tc_kimlik');
            $table->string('telefon')->nullable();
            $table->date('ziyaret_tarihi');
            $table->string('giris_saati');
            $table->string('ziyaret_edilen');
            $table->text('ziyaret_sebebi');
            $table->string('cikis_saati')->nullable();
            $table->string('belge');
            $table->string('firma')->nullable();
            $table->string('arac_plaka')->nullable();
            $table->integer('kart_no')->nullable();
            $table->integer('kurum_id');
            $table->integer('birim_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ziyaretci_log');
    }
}
