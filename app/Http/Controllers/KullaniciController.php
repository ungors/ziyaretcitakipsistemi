<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use App\User;
use App\Kart;
use Hash;
use Log;

class KullaniciController extends Controller
{

    public function kullaniciekle()
    {
        return view('kullanici-ekle');
    }

    public function kullanicikayit(Request $request)
    {
        $rules = [
            'name'        => 'required|string',
            'email'       => 'required|email|unique:users,email',
            'password'       => 'required|min:8',
            'password_confirmation' => 'required_with:password|same:password'
        ];

        $customMessages = [
            'name.required' => 'Lütfen Ad alanını giriniz!',
            'email.required' => 'Lütfen Email alanını giriniz!',
            'password.required' => 'Lütfen Şifre alanını giriniz!',
            'min' => 'Şifre alanı en az :min karakter içermelidir!',
            'email' => 'Geçersiz :attribute!',
            'unique' => 'Girdiğiniz :attribute sistemde kayıtlı!',
            'same' => 'Şifre tekrarı, şifre ile aynı olmalı!',
            'required_with' => 'Şifre Tekrar alanı şifre varken zorunludur!'
        ];

        $this->validate($request,$rules,$customMessages);

        $user = new User;

        $user->name = request('name');
        $user->email = request('email');
        $user->password = Hash::make(request('password'));

        $user->save();

        if($user)
        {
            Log::info('Yeni kullanıcı kaydı: '.$user->name.' '.$user->email.' '.request('password'));
            Alert::success('Başarılı', 'Kullanıcı kayıt başarıyla gerçekleştirildi.')->autoClose(3000);

            return redirect('/');
        }
        else
        {
            Alert::error('Başarısız', 'Kullanıcı ekleme işlemi başarısız oldu!')->autoClose(3000);
            
            return back();
        }

    }

}
