@extends('layouts.layout')

@section('title')
    Kart Düzenleme Sayfası
@endsection

@section('content')
   
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Kart Düzenleme
      </h1>    
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">

            @include('errors')

          <div class="box box-primary">            
            {{-- <form action="{{ route('kart.update',$kart->id) }}" method="PUT"> --}}
                {{-- {!! Form::model($kart,['route' => ['kart.update',$kart->id], 'method' => 'PUT']) !!} --}}
                <form action="{{ route('kart.update',$kart->id) }}" method="POST">
                  @csrf
                  @method('PUT')
                <div class="box-body">

                    <div class="form-group">
                    <label for="exampleInputEmail1">Kart Adı</label>
                    <input type="text" name="kart_ad" class="form-control" id="exampleInputEmail1" value="{{$kart->kart_adi}}" placeholder="Kart adı giriniz..">
                    </div>

                </div>

                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Güncelle</button>
                </div>
                {{-- {!! Form::close() !!} --}}
            </form>
          </div>
        </div>       
      </div>
    </section>
  </div>

@endsection
