@extends('layouts.layout')

@section('title')
    Bugünkü Ziyaretçiler
@endsection

@section('content')

<div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Bugünkü Ziyaretçiler
          </h1>
        </section>
        <!-- Main content -->
        
        <section class="content">
          
          <!-- /.row -->
          <div class="row">
            <div class="col-xs-12">
             @if(count($ziyaretciler) > 0)
              <div class="box">
                <div class="box-header">
                  <div class="box-tools">
                   
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>AD</th>
                            <th>SOYAD</th>
                            {{-- <th>TC Kimlik No</th> --}}
                            <th>Telefon Numarası</th>
                            <th>Tarih</th>
                            <th>Giriş Saati</th>
                            <th>Çıkış Saati</th>
                            <th>Ziyaret Edilen Kişi</th>
                            <th>Ziyaret Sebebi</th>
                            {{-- <th>Durum</th> --}}
                            <th>Çıkış Yap</th>
                            <th>Detay</th>
                            <th>Düzenle</th>
                          </tr>
                        </thead>
                    <tbody>

                      @foreach ($ziyaretciler as $ziyaretci)
                      <tr>
                          <td>{{$ziyaretci->ad}}</td>
                          <td>{{$ziyaretci->soyad}}</td>
                          {{-- <td>{{$ziyaretci->tc_kimlik}}</td> --}}
                          <td>{{$ziyaretci->telefon}}</td>
                          <td>{{ Carbon\Carbon::parse($ziyaretci->ziyaret_tarihi)->format('d-m-Y') }}</td>
                          <td>{{$ziyaretci->giris_saati}}</td>
                          <td>
                              {{$ziyaretci->cikis_saati}}
                          </td>
                          <td>{{$ziyaretci->ziyaret_edilen}}</td>
                          <td>{{$ziyaretci->ziyaret_sebebi}}</td>
                          {{-- <td>
                              @if (!empty($ziyaretci->cikis_saati))
                                <span class="btn btn-success">Çıkış Yaptı</span>
                              @else 
                                <span class="btn btn-warning">İçerde</span>
                              @endif
                          </td> --}}
                          <td>
                              @if (empty($ziyaretci->cikis_saati))
                                <a class="btn btn-success" href="{{ route('ziyaretci.cikis', $ziyaretci->id) }}">Çıkış</a>
                              @else 
                                <span class="btn btn-danger">Çıkış Yaptı</span>
                              @endif
                          </td>
                          <td>
                              <a class="btn btn-primary" href="{{route('ziyaretci.show',$ziyaretci->id)}}">Detay</a>
                          </td>
                          <td>
                              <a class="btn btn-success" href="{{route('ziyaretci.edit',$ziyaretci->id)}}">Düzenle</a>
                          </td>
                      </tr>
                      @endforeach
                      
                    </tbody>
                  </table>
                  {{ $ziyaretciler->links() }}
                </div>
                <!-- /.box-body -->
              </div>
              @else
              <br>
                <div class="alert alert-warning">Kayıt bulunamadı.</div>
              @endif
      
              <!-- /.box -->
            </div>
          </div>
        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->
    

@endsection