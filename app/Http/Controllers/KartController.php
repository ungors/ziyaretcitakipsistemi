<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kartlar;
use RealRashid\SweetAlert\Facades\Alert;
use Log;

class KartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kartlar = Kartlar::paginate(10);
        return view('kartlar',compact('kartlar'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('kart-ekle');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'kart_ad'        => 'required|string|unique:kartlar,kart_adi',
        ];

        $customMessages = [
            'kart_ad.required' => 'Lütfen Kart Adı alanını giriniz!',
            'kart_ad.string' => 'Kart Adı alanına sadece metinsel değer giriniz!',
            'kart_ad.unique' => 'Bu kart adı daha önce eklenmiş!'
        ];

        $this->validate($request,$rules,$customMessages);

        $kart = new Kartlar;
        $kart->kart_adi = request('kart_ad');

        $kart->save();

        if($kart)
        {
            Log::info('Yeni kart eklendi: '.$kart->kart_adi);
            Alert::success('Başarılı', 'Kart başarıyla eklendi.')->autoClose(3000);

            return redirect()->route('kart.index');
        }
        else
        {
            Alert::error('Başarısız', 'Kart ekleme işlemi başarısız!')->autoClose(3000);

            return back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id)
    {
        if(!$id)
        {
            return back();
        }

        $id = intval($id);

        $kart = Kartlar::findOrFail($id);
        return view('kart-duzenle',compact('kart'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!$id)
        {
            return back();
        }

        $id = intval($id);

        $rules = [
            'kart_ad'        => 'required|string',
        ];

        $customMessages = [
            'kart_ad.required' => 'Lütfen Kart Adı alanını giriniz!',
            'kart_ad.string' => 'Kart Adı alanına sadece metinsel değer giriniz!',
            // 'kart_ad.unique' => 'Bu kart adı daha önce eklenmiş!'
        ];

        $this->validate($request,$rules,$customMessages);

        $kart = Kartlar::findOrFail($id);
        $kart->kart_adi = request('kart_ad');

        $kart->save();

        if($kart)
        {
            Alert::success('Başarılı', 'Kart başarıyla güncellendi.')->autoClose(3000);
        }
        else
        {
            Alert::error('Başarısız', 'Kart güncelleme işlemi başarısız!')->autoClose(3000);
        }

        return back();
        // return redirect()->route('kart.index');

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // if(!$id)
        // {
        //     return back();
        // }

        // $id = intval($id);

        // $sil = Kartlar::destroy($id);

        // if($sil)
        // {
        //     Alert::success('Başarılı', 'Kart başarıyla silindi.')->autoClose(3000);
        // }
        // else
        // {
        //     Alert::error('Başarısız', 'Kart silme işlemi başarısız!')->autoClose(3000);
        // }

        // return back();

    }
}
