<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKurumIdToZiyaretcilerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ziyaretciler', function (Blueprint $table) {
            $table->integer('kurum_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ziyaretciler', function (Blueprint $table) {
            $table->dropColumn('kurum_id');
        });
    }
}
