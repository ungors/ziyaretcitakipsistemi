<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kartlar extends Model
{
    protected $table = 'kartlar';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function ziyaretciler()
    {
        return $this->hasMany('App\Ziyaretci','kart_no,','id');
    }
}
