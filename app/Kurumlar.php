<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kurumlar extends Model
{
    protected $table = 'birimler';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function ziyaretciler()
    {
        return $this->hasMany('App\Ziyaretci','kurum_id,','id');
    }
}
