<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use RealRashid\SweetAlert\Facades\Alert;
use App\Ziyaretci;
use App\Kartlar;
use App\Kurumlar;
use App\ZiyaretciLog;
use Carbon;
use Log;

class ZiyaretController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ziyaretciler = Ziyaretci::where('ziyaret_tarihi',date('Y-m-d'))->orderBy('giris_saati', 'desc')->paginate(10);

        return view('ziyaretciler-bugun',compact('ziyaretciler'));
    }

    public function ziyaretci_dun()
    {
        $tarih = Carbon\Carbon::now()->addDay(-1)->format('Y-m-d');
        $ziyaretciler = Ziyaretci::where('ziyaret_tarihi',$tarih)->orderBy('giris_saati', 'desc')->paginate(10);

        return view('ziyaretciler-dun',compact('ziyaretciler'));
    }

    public function ziyaretci_hafta()
    {
        $from = Carbon\Carbon::now()->startOfWeek()->format('Y-m-d'); 
        $to = date('Y-m-d'); 
        $ziyaretciler = Ziyaretci::whereBetween('ziyaret_tarihi', [$from, $to])->orderBy('ziyaret_tarihi', 'desc')->orderBy('giris_saati', 'desc')->paginate(10);
        return view('ziyaretciler-hafta',compact('ziyaretciler'));
    }

    public function ziyaretci_ay()
    {
        $from = Carbon\Carbon::now()->startOfMonth()->format('Y-m-d'); 
        $to = date('Y-m-d'); 
        $ziyaretciler = Ziyaretci::whereBetween('ziyaret_tarihi', [$from, $to])->orderBy('ziyaret_tarihi', 'desc')->orderBy('giris_saati', 'desc')->paginate(10);

        return view('ziyaretciler-ay',compact('ziyaretciler'));
    }

    public function ziyaretci_tum()
    {
        $from = Carbon\Carbon::now()->startOfYear()->format('Y-m-d');
        
        $to = date('Y-m-d'); 
        $ziyaretciler = Ziyaretci::whereBetween('ziyaret_tarihi', [$from, $to])->orderBy('ziyaret_tarihi', 'desc')->orderBy('giris_saati', 'desc')->paginate(10);

        return view('ziyaretciler-tum',compact('ziyaretciler'));
    }

    public function birim_getir(Request $request)
    {
        $birimler = DB::table("birimler")
            ->where("ust_kurum_id",$request->kurum_id)
            ->pluck("kurum_adi","id");
            return response()->json($birimler);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kartlar = Kartlar::all();
        $kurumlar = Kurumlar::where('ust_kurum_id',NULL)->get();
        return view('ziyaretci-ekle',compact('kartlar','kurumlar'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'ad'        => 'required|string|max:25',
            'soyad'    => 'required|string|max:30',
            'belge'    => 'required|string|max:50',
            'kimlik_no'  => 'required|numeric',
            'telefon' =>     'nullable|numeric|digits_between:10,11',
            'firma'   => 'nullable|string|max:100',
            'plaka'   => 'nullable|string|max:20',
            'ziyaret_edilen' => 'required|string|max:100',
            'ziyaret_sebebi' => 'required|string|max:300',
        ];

        $customMessages = [
            'ad.required' => 'Lütfen Ad alanını boş bırakmayınız.',
            'soyad.required' => 'Lütfen Soyad alanını boş bırakmayınız.',
            'belge.required' => 'Lütfen Belge Türü alanını boş bırakmayınız.',
            'kimlik_no.required' => 'Lütfen Kimlik Numarası alanını boş bırakmayınız.',
            'ziyaret_edilen.required' => 'Lütfen ziyaret edilen kişiyi giriniz.',
            'ziyaret_sebebi.required' => 'Lütfen ziyaret sebebi giriniz.',
            'ad.max' => 'Ad alanı en fazla :max karakter içermelidir!',
            'soyad.max' => 'Soyad alanı en fazla :max karakter içermelidir!',
            'belge.max' => 'Belge Türü alanı en fazla :max karakter içermelidir!',
            'firma.max' => 'Firma Adı alanı en fazla :max karakter içermelidir!',
            'plaka.max' => 'Plaka alanı en fazla :max karakter içermelidir!',
            'kimlik_no.numeric' => 'Kimlik Numarası alanına sadece rakam giriniz!',
            'telefon.numeric' => 'Telefon alanına sadece rakam giriniz!',
            'telefon.digits_between' => 'Telefon alanını eksiksiz giriniz!',
            'ziyaret_edilen.max' => 'Ziyaret Edilen alanı en fazla :max karakter olmalıdır!',
            'ziyaret_sebebi.max' => 'Ziyaret Sebebi alanı en fazla :max karakter olmalıdır!',
            'ad.string' => 'Ad alanı metinsel olmalıdır!',
            'soyad.string' => 'Soyad alanı metinsel olmalıdır!',
            'ziyaret_edilen.string' => 'Ziyaret Edilen alanı metinsel olmalıdır!',
            'ziyaret_sebebi.string' => 'Ziyaret Sebebi alanı metinsel olmalıdır!',
            'firma.string' => 'Firma Adı alanı metinsel olmalıdır!',
            'plaka.string' => 'Plaka alanı metinsel olmalıdır!',           
            'belge.string' => 'Belge Türü alanı metinsel olmalıdır!'
        ];

        $this->validate($request,$rules,$customMessages);

        $ziyaretci = new Ziyaretci;
        
        $ziyaretci->ad = request('ad');
        $ziyaretci->soyad = request('soyad');
        $ziyaretci->belge = request('belge');
        $ziyaretci->tc_kimlik = request('kimlik_no');
        $ziyaretci->telefon = request('telefon');
        $ziyaretci->ziyaret_tarihi = date('Y-m-d');
        $ziyaretci->giris_saati = date('H:i');
        $ziyaretci->firma = request('firma');
        $ziyaretci->arac_plaka = request('plaka');
        $ziyaretci->kurum_id = request('kurum');
        $ziyaretci->birim_id = request('birim');   
        $ziyaretci->seflik_id = request('seflik'); 
        $ziyaretci->ziyaret_edilen = request('ziyaret_edilen');
        $ziyaretci->ziyaret_sebebi = request('ziyaret_sebebi');
        $ziyaretci->kart_no = request('kart-adi');
        
        $ziyaretci->save();

        if($ziyaretci)
        {
            //Ziyaretçi Log tablosu oluşturulur.
            $log = new ZiyaretciLog;

            $log->id = $ziyaretci->id;
            $log->ad = request('ad');
            $log->soyad = request('soyad');
            $log->belge = request('belge');
            $log->tc_kimlik = request('kimlik_no');
            $log->telefon = request('telefon');
            $log->ziyaret_tarihi = date('Y-m-d');
            $log->giris_saati = date('H:i');
            $log->firma = request('firma');
            $log->arac_plaka = request('plaka');
            $log->kurum_id = request('kurum');
            $log->birim_id = request('birim');  
            $log->seflik_id = request('seflik');  
            $log->ziyaret_edilen = request('ziyaret_edilen');
            $log->ziyaret_sebebi = request('ziyaret_sebebi');
            $log->kart_no = request('kart-adi');

            $log->save();

            //Loglama işlemi
            Log::info('Yeni ziyaretçi: '.
            'ID: '.$ziyaretci->id.
            ' Ad: '.$ziyaretci->ad.' Soyad: '.$ziyaretci->soyad.
            ' Belge: '.$ziyaretci->belge.' Kimlik No: '.$ziyaretci->tc_kimlik.
            ' Telefon: '.$ziyaretci->telefon.' Ziyaret Tarihi: '.$ziyaretci->ziyaret_tarihi.
            ' Giriş Saati: '.$ziyaretci->giris_saati.' Firma: '.$ziyaretci->firma.
            ' Plaka: '.$ziyaretci->arac_plaka.' Kurum: '.$ziyaretci->kurum->kurum_adi.
            ' Ziyaret edilen: '.$ziyaretci->ziyaret_edilen.' Ziyaret sebebi: '.$ziyaretci->ziyaret_sebebi.
            ' Kart No: '.$ziyaretci->kart_no
            );

            Alert::success('Başarılı', 'Ziyaretçi başarıyla eklendi.')->autoClose(3000);

            return redirect()->route('ziyaretci.index');
        }
        else
        {
            Alert::error('Başarısız', 'Ziyaretçi ekleme işlemi başarısız oldu!')->autoClose(3000);
            
            return back();
        }
    }

    public function cikis_yaptir($id)
    {
        if(!$id)
        {
            return back();
        }

        $id = intval($id);

        $cikis = Ziyaretci::where([
            ["id", "=" ,$id]
        ])->update([
            "cikis_saati" => date('H:i')
        ]);

        if($cikis)
        {
            Log::info('Id: '.$id.' olan ziyaretçi çıkış yaptı.');
            Alert::success('Başarılı', 'Ziyaretçinin çıkışı yapıldı.')->autoClose(3000);
        }
        else
        {
            Alert::error('Başarısız', 'Ziyaretçi çıkış işlemi başarısız!')->autoClose(3000);
        }

        return redirect()->route('ziyaretci.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(!$id)
        {
            return back();
        }

        $id = intval($id);

        $ziyaretci = Ziyaretci::findOrFail($id);
        $kart = Kartlar::find($ziyaretci->kart_no);
        $birim = Kurumlar::find($ziyaretci->birim_id);
        $seflik = Kurumlar::find($ziyaretci->seflik_id);
        return view('ziyaretci-detay',compact('ziyaretci','kart','birim','seflik'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!$id)
        {
            return back();
        }

        $id = intval($id);

        $ziyaretci = Ziyaretci::findOrFail($id);
        $kurumlar = Kurumlar::where('ust_kurum_id',NULL)->get();
        $kartlar = Kartlar::all();
        $birim = Kurumlar::find($ziyaretci->birim_id);
        $alt_birimler = Kurumlar::where('ust_kurum_id',$ziyaretci->kurum_id)->get();
        $seflikler = Kurumlar::where('ust_kurum_id',$ziyaretci->birim_id)->get();

        return view('ziyaretci-duzenle',compact('ziyaretci','kurumlar','kartlar','birim','alt_birimler','seflikler'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!$id)
        {
            return back();
        }

        $id = intval($id);

        $rules = [
            'ad'        => 'required|string|max:25',
            'soyad'    => 'required|string|max:30',
            'belge'    => 'required|string|max:50',
            'kimlik_no'  => 'required|numeric',
            'telefon' =>     'nullable|numeric|digits_between:10,11',
            'firma'   => 'nullable|string|max:100',
            'plaka'   => 'nullable|string|max:20',
            'ziyaret_edilen' => 'required|string|max:100',
            'ziyaret_sebebi' => 'required|string|max:300',
        ];

        $customMessages = [
            'ad.required' => 'Lütfen Ad alanını boş bırakmayınız.',
            'soyad.required' => 'Lütfen Soyad alanını boş bırakmayınız.',
            'belge.required' => 'Lütfen Belge Türü alanını boş bırakmayınız.',
            'kimlik_no.required' => 'Lütfen Kimlik Numarası alanını boş bırakmayınız.',
            'ziyaret_edilen.required' => 'Lütfen ziyaret edilen kişiyi giriniz.',
            'ziyaret_sebebi.required' => 'Lütfen ziyaret sebebi giriniz.',
            'ad.max' => 'Ad alanı en fazla :max karakter içermelidir!',
            'soyad.max' => 'Soyad alanı en fazla :max karakter içermelidir!',
            'belge.max' => 'Belge Türü alanı en fazla :max karakter içermelidir!',
            'firma.max' => 'Firma Adı alanı en fazla :max karakter içermelidir!',
            'plaka.max' => 'Plaka alanı en fazla :max karakter içermelidir!',
            'kimlik_no.numeric' => 'Kimlik Numarası alanına sadece rakam giriniz!',
            'telefon.numeric' => 'Telefon alanına sadece rakam giriniz!',
            'telefon.digits_between' => 'Telefon alanını eksiksiz giriniz!',
            'ziyaret_edilen.max' => 'Ziyaret Edilen alanı en fazla :max karakter olmalıdır!',
            'ziyaret_sebebi.max' => 'Ziyaret Sebebi alanı en fazla :max karakter olmalıdır!',
            'ad.string' => 'Ad alanı metinsel olmalıdır!',
            'soyad.string' => 'Soyad alanı metinsel olmalıdır!',
            'ziyaret_edilen.string' => 'Ziyaret Edilen alanı metinsel olmalıdır!',
            'ziyaret_sebebi.string' => 'Ziyaret Sebebi alanı metinsel olmalıdır!',
            'firma.string' => 'Firma Adı alanı metinsel olmalıdır!',
            'plaka.string' => 'Plaka alanı metinsel olmalıdır!',           
            'belge.string' => 'Belge Türü alanı metinsel olmalıdır!'
        ];

        $this->validate($request,$rules,$customMessages);

        $ziyaretci = Ziyaretci::findOrFail($id);

        $ziyaretci->ad = request('ad');
        $ziyaretci->soyad = request('soyad');
        $ziyaretci->belge = request('belge');
        $ziyaretci->tc_kimlik = request('kimlik_no');
        $ziyaretci->telefon = request('telefon');
        $ziyaretci->ziyaret_tarihi = $ziyaretci->getOriginal('ziyaret_tarihi');
        $ziyaretci->giris_saati = $ziyaretci->getOriginal('giris_saati');
        $ziyaretci->firma = request('firma');
        $ziyaretci->arac_plaka = request('plaka');
        $ziyaretci->kurum_id = request('kurum');
        $ziyaretci->birim_id = request('birim');
        $ziyaretci->seflik_id = request('seflik'); 
        $ziyaretci->ziyaret_edilen = request('ziyaret_edilen');
        $ziyaretci->ziyaret_sebebi = request('ziyaret_sebebi');
        $ziyaretci->kart_no = request('kart-no');

        $ziyaretci->save();

        if($ziyaretci)
        {
            Alert::success('Başarılı', 'Ziyaretçi başarıyla güncellendi.')->autoClose(3000);
        }
        else
        {
            Alert::error('Başarısız', 'Ziyaretçi güncelleme işlemi başarısız!')->autoClose(3000);
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
