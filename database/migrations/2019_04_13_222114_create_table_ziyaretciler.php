<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableZiyaretciler extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ziyaretciler', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ad');
            $table->string('soyad');
            $table->integer('tc_kimlik');
            $table->string('telefon');
            $table->date('ziyaret_tarihi');
            $table->string('giris_saati');
            $table->string('ziyaret_edilen');
            $table->text('ziyaret_sebebi');
            $table->string('cikis_saati')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ziyaretciler');
    }
}
