@extends('layouts.layout')

@section('title')
    Kart Ekleme Sayfası
@endsection

@section('content')
   
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Kart Ekle
      </h1>    
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">

            @include('errors')

          <div class="box box-primary">            
            <form action="{{ route('kart.store') }}" method="POST">
              @csrf

            <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Kart Adı</label>
                  <input type="text" name="kart_ad" class="form-control" id="exampleInputEmail1" placeholder="Kart adı giriniz..">
                </div>
            </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Ekle</button>
            </div>

              </form>
          </div>
        </div>       
      </div>
    </section>
  </div>

@endsection
