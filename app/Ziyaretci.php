<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ziyaretci extends Model
{
    protected $table = 'ziyaretciler';
    public $primaryKey = 'id';
    public $timestamps = true;

    public function kurum()
    {
        return $this->belongsTo('App\Kurumlar');
    }

    public function kart()
    {
        return $this->belongsTo('App\Kartlar');
    }


}
