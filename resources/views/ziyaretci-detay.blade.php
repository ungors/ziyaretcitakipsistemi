@section('title')
    Ziyaretçi Detay Sayfası
@endsection

@extends('layouts.layout')

@section('content')
<div class="content-wrapper">
    <section class="content-header">
      <h1>
        Ziyaretçi Detay
      </h1>    
    </section>

    <section class="content">
        <div class="row">
          <div class="col-md-12">
                <div class="box box-primary">
                        <div class="box-header with-border">
                          <h3 class="box-title">Ziyaretçi Bilgileri</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                          <strong><i class="fa fa-user"></i> Ad-Soyad</strong>
            
                          <p class="text-muted">
                            {{$ziyaretci->ad}} {{$ziyaretci->soyad}}
                          </p>

                          <hr>
          
                        <strong><i class="fa fa-file-text-o margin-r-5"></i> Belge</strong>
                        
                            <p class="text-muted">{{$ziyaretci->belge}}</p>
            
                          <hr>
            
                          <strong><i class="fa fa-id-card" aria-hidden="true"></i> Belge Numarası</strong>
            
                          <p class="text-muted">{{$ziyaretci->tc_kimlik}}</p>
            
                          <hr>
            
                          <strong><i class="fa fa-phone" aria-hidden="true"></i> Telefon</strong>
            
                           <p class="text-muted">{{$ziyaretci->telefon}}</p>

                        </div>
                        <!-- /.box-body -->
                      </div>
          </div>
          <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Tarih-Zaman Bilgileri</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <strong><i class="fa fa-calendar" aria-hidden="true"></i> Ziyaret Tarihi</strong>
    
                  <p class="text-muted">
                        {{ Carbon\Carbon::parse($ziyaretci->ziyaret_tarihi)->format('d-m-Y') }}
                  </p>
    
                  <hr>
    
                  <strong><i class="fa fa-clock-o" aria-hidden="true"></i> Giriş Saati</strong>
    
                  <p class="text-muted">{{$ziyaretci->giris_saati}}</p>
    
                  <hr>
    
                  <strong><i class="fa fa-clock-o" aria-hidden="true"></i> Çıkış Saati</strong>
    
                  <p class="text-muted">{{$ziyaretci->cikis_saati}}</p>
                </div>
                <!-- /.box-body -->
            </div>
          </div>
        
          <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Ziyaret Bilgileri</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                  <strong><i class="fa fa-user-o" aria-hidden="true"></i> Ziyaret Edilen Birim</strong>
  
                  <p class="text-muted">
                        {{ $ziyaretci->kurum->kurum_adi }}
                  </p>
    
                  <hr>

                  <strong><i class="fa fa-user-o" aria-hidden="true"></i> Ziyaret Edilen Müdürlük</strong>
  
                  <p class="text-muted">
                    @if ($birim)
                      {{ $birim->kurum_adi }}
                    @else
                        Birim bilgisi girilmemiş.
                    @endif
                  </p>
    
                  <hr>

                  <strong><i class="fa fa-user-o" aria-hidden="true"></i> Ziyaret Edilen Şeflik</strong>
  
                  <p class="text-muted">
                    @if ($seflik)
                      {{ $seflik->kurum_adi }}
                    @else
                        Şeflik bilgisi girilmemiş.
                    @endif
                  </p>
    
                  <hr>

                  <strong><i class="fa fa-user-o" aria-hidden="true"></i> Ziyaret Edilen Kişi</strong>
    
                  <p class="text-muted">
                        {{ $ziyaretci->ziyaret_edilen }}
                  </p>
    
                  <hr>
    
                  <strong><i class="fa fa-pencil" aria-hidden="true"></i> Ziyaret Sebebi</strong>
    
                  <p class="text-muted">{{$ziyaretci->ziyaret_sebebi}}</p>

                </div>
                <!-- /.box-body -->
              </div>
          </div>
          <div class="col-md-12">
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Diğer Bilgiler</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <strong><i class="fa fa-building" aria-hidden="true"></i> Ziyaretçinin Firması</strong>
    
                  <p class="text-muted">
                        {{$ziyaretci->firma}} 
                    </p>
    
                  <hr>
    
                  <strong><i class="fa fa-car" aria-hidden="true"></i> Araç Plakası</strong>
    
                  <p class="text-muted">{{$ziyaretci->arac_plaka}}</p>
    
                  <hr>
    
                  <strong><i class="fa fa-address-card-o" aria-hidden="true"></i> Kart No</strong>
    
                    <p class="text-muted">
                      @if ($kart)
                        {{ $kart->kart_adi }} 
                      @else
                         Kart bilgisi girilmemiş. 
                      @endif
                    </p>
                  
                </div>
                <!-- /.box-body -->
              </div>
        </div>
        </div>
    </section>  
  </div>
@endsection