<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Route::group(['prefix'=>'admin','middleware'=>'admin'],function(){

Route::resource('ziyaretci','ZiyaretController');

Route::resource('kart','KartController');

Route::get('/cikis-yaptir/{id}','ZiyaretController@cikis_yaptir')->name('ziyaretci.cikis');

Route::get('/ziyaretciler-dun', 'ZiyaretController@ziyaretci_dun')->name('ziyaretci.dun');

Route::get('/ziyaretciler-hafta', 'ZiyaretController@ziyaretci_hafta')->name('ziyaretci.hafta');

Route::get('/ziyaretciler-ay', 'ZiyaretController@ziyaretci_ay')->name('ziyaretci.ay');

Route::get('/ziyaretciler-tum', 'ZiyaretController@ziyaretci_tum')->name('ziyaretci.tum');

Route::get('birim-getir','ZiyaretController@birim_getir')->name('birim.getir');

});


Route::get('/kullanici-ekle', 'KullaniciController@kullaniciekle')->name('kullanici.ekle');

Route::post('/kullanicikayit','KullaniciController@kullanicikayit')->name('kullanici.kayit');

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

