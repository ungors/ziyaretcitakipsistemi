<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>@yield('title')</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  
  <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
  
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{URL::asset('css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{URL::asset('css/ionicons.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{URL::asset('css/AdminLTE.min.css')}}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{URL::asset('css/_all-skins.min.css')}}">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="{{ route('ziyaretci.index') }}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>Z</b>TS</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Ziyaretçi</b> Takip</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- Notifications: style can be found in dropdown.less -->
          
         
          <!-- User Account: style can be found in dropdown.less -->
          {{-- <li class="dropdown user user-menu">

              <form id="logout-form" action="{{ route('logout') }}" method="POST" class="form-horizontal">
                  @csrf
                  <a style="height:100%;background-color:red;color:#fff;" href="javascript:;" onclick="parentNode.submit();">Çıkış Yap</a>  
              </form>  
         
          </li> --}}

          <li class="dropdown user user-menu">
              <form name="logout-form" id="logout-form" action="{{ route('logout') }}" method="POST" class="form-horizontal">
                  @csrf
              </form>  
              <a href="javascript:;" onclick="$('#logout-form').submit();">
                <span class="hidden-xs">Çıkış Yap</span>
              </a>
            </li>
          <!-- Control Sidebar Toggle Button -->
         
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">  
        <li>
          <a href="{{ route('ziyaretci.create') }}">
              <i class="fa fa-plus" aria-hidden="true"></i> <span>Ziyaretçi Giriş Ekle</span>
          </a>
        </li>
        <li>
          <a href="{{ route('ziyaretci.index') }}">
              <i class="fa fa-address-book" aria-hidden="true"></i> <span>Ziyaretçiler (Bugün)</span>
          </a>
        </li>
        <li>
          <a href="{{ route('ziyaretci.dun') }}">
              <i class="fa fa-address-book" aria-hidden="true"></i> <span>Ziyaretçiler (Dün)</span>
          </a>
        </li>
        <li>
          <a href="{{ route('ziyaretci.hafta') }}">
            <i class="fa fa-address-book" aria-hidden="true"></i> <span>Ziyaretçiler (Bu Hafta)</span>
          </a>
        </li>
        <li>
          <a href="{{ route('ziyaretci.ay') }}">
            <i class="fa fa-address-book" aria-hidden="true"></i> <span>Ziyaretçiler (Bu Ay)</span>
          </a>
        </li>
        <li>
          <a href="{{ route('ziyaretci.tum') }}">
            <i class="fa fa-address-book" aria-hidden="true"></i> <span>Ziyaretçiler (Tüm)</span>
          </a>
        </li>
        <li>
          <a href="{{ route('kart.index') }}">
            <i class="fa fa-address-card-o" aria-hidden="true"></i> <span>Kartlar</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

@yield('content')

  <footer style="text-align:center;" class="main-footer">
    <strong><a href="http://www.balikesir-baski.gov.tr/">BASKİ</a>&copy; 2019 Ziyaretçi Takip</strong> 
  </footer>

  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

@include('sweetalert::alert')
<!-- jQuery 3 -->
<script src="{{URL::asset('js/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script>
<!-- SlimScroll -->
<script src="{{URL::asset('js/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{URL::asset('js/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{URL::asset('js/adminlte.min.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{URL::asset('js/demo.js')}}"></script>
<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })
</script>
</body>
</html>
