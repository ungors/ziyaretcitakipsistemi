<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSeflikIdToZiyaretcilerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ziyaretciler', function (Blueprint $table) {
            $table->integer('seflik_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ziyaretciler', function (Blueprint $table) {
            $table->dropColumn('seflik_id');
        });
    }
}
